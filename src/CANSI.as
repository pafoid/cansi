package
{
	import com.pafoid.ui.AppBase;
	import com.pafoid.ui.IAppFacade;
	import com.pafoid.ui.IApplication;
	
	import app.CansiAppFacade;
	import app.ViewIds;
	
	public class CANSI extends AppBase implements IApplication
	{
		public function CANSI()
		{
			super();
			
			appFacade.viewManager.changeView(ViewIds.LOGIN_VIEW);
		}
		
		override public function get appFacade():IAppFacade{
			_appFacade ||= new CansiAppFacade(this);
			return _appFacade;
		}
	}
}