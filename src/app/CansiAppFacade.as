package app
{
	import com.pafoid.ui.IAppFacade;
	import com.pafoid.ui.IApplication;
	import com.pafoid.ui.IViewManager;
	
	import view.CansiViewManager;
	
	public class CansiAppFacade implements IAppFacade
	{
		private var _app:IApplication;
		private var _viewManager:CansiViewManager;
		
		
		public function CansiAppFacade(application:IApplication)
		{
			_app = application;
			_viewManager = new CansiViewManager(this);
		}
		
		//Getters/Setters
		public function get app():IApplication{
			return _app;
		}
		
		public function set app(value:IApplication):void{
			_app = value;
		}
		
		public function get viewManager():IViewManager{
			return _viewManager;
		}
		
		public function destroy():void{
		}
	}
}