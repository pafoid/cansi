package view
{
	import com.pafoid.ui.IAppFacade;
	import com.pafoid.ui.IView;
	import com.pafoid.ui.IViewManager;
	import com.pafoid.ui.ViewManagerBase;
	
	import app.ViewIds;
	
	public class CansiViewManager extends ViewManagerBase implements IViewManager
	{
		public function CansiViewManager(appFacade:IAppFacade)
		{
			super(appFacade);
		}
		
		override protected function initViews():void{
			_views[ViewIds.LOGIN_VIEW] = new LoginView(_appFacade);				
		} 
		
		override public function changeView(viewId:String):void{
			if(!_currentView){
				_currentView = _views[viewId];
				addView(_currentView);
			}else{
				var tempView:IView = _views[viewId];
				addView(tempView);
				
				removeView(_currentView);
				_currentView = tempView;
			}
		}
	}
}