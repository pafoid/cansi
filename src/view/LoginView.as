package view
{
	import com.pafoid.ui.BaseView;
	import com.pafoid.ui.IAppFacade;
	
	import flash.display.Sprite;
	import flash.text.TextField;
	
	public class LoginView extends BaseView
	{
		private var _titleTf:TextField;
		private var _userNameLabel:TextField;
		private var _userNameInput:TextField;
		private var _passwordNameLabel:TextField;
		private var _passwordInput:TextField;
		private var _connectButton:Sprite;
		
		public function LoginView(appFacade:IAppFacade)
		{
			super(appFacade);
			
			initUI();
		}
		
		private function initUI():void
		{
			
		}
		
		override public function destroy():void{
			super.destroy();	
		}
	}
}